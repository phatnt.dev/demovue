using System;
using ServiceStack;
using DemoProject.ServiceModel;

namespace DemoProject.ServiceInterface
{
    public class MyServices : Service
    {
        public object Any(Hello request)
        {
            return new HelloResponse { Result = $"Hello, {request.Name}!" };
        }
    }
}
